// Не важно где обьявлена функция. Важно кто ее вызывает !!!

// this - контектс вызова
// this - это тот кто вызывает функцию

function sum(a, b) {
    console.log(a + b);
    console.log('sum this', this);
}

// sum(5, 10);
// window.sum(20, 30);
// this.sum(100, 200);

// alert('Hello');
// window.alert('widow hello');
// this.alert('this hello');

// window.sum(50, 100);
// this.sum(50, 100); // точка есть всегда, но или мы ее указываем млм сам JS ставит по дефолту

// let name = 'Vova',

// function showThis() {
//     console.log(this);
// }

// let userVova = {
//     // property
//     name: 'Vova',
//     age: 25,

//     // Method
//     showThis() {
//         console.log(this);
//     },

//     getInfo() {
//         this.showThis();
//         console.log(this.name, this.age);
//     },
// };

// userVova.showThis();
// userVova.getInfo();

// let userSara = {
//     // property
//     name: 'Sara',
//     age: 45,

//     // Method
//     showThis() {
//         console.log(this);
//     },

//     getInfo() {
//         this.showThis();
//         console.log(this.name, this.age);
//         // this.sum(50, 100);// ошибка
//         // sum(50, 100);// window
//     },
// };

// userSara.showThis();
// userSara.getInfo();
//=====================================

let userVova = {
    name: 'Vova',
    age: 25,

    getInfo() {
        console.log(this.name, this.age);
        console.log(this);
    },
};

// userVova.getInfo();

let userSara = {
    name: 'Sara',
    age: 21,
};

let userBob = {
    name: 'Bob',
    age: 50,
};

// Возьми мне метод getInfo обьекта userVova и вызови его на обьекте userSara

// userVova.getInfo.call(userSara);
// userVova.getInfo.call(userBob);
// userVova.getInfo.apply(userSara);
// userVova.getInfo.bind(userSara)();

// ============================================

// let btn = document.querySelector('button');
// console.log(btn);

// btn.addEventListener('click', () => {
//     console.log(this);
// });

// btn.addEventListener('click', function () {
//     console.log(this);
// });

// btn.addEventListener('click', userVova.getInfo.bind(userVova));
// btn.addEventListener('click', userVova.getInfo.bind(userSara));
