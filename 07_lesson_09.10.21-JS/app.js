// let xhr = XMLHttpRequest;

// ES5
// function User {

// }

// let vova = new User()

// ES6
// class User {

// }

// let sara = new Sara()

// 2015
// new  promise = new Promise()
// promise.then().catch()

// async/await

// async function User {
//     await request = fetch()
//     await request = fetch()
//     await request = fetch()
//     await request = fetch()
//     await request = fetch()
//     await request = fetch()
// }
