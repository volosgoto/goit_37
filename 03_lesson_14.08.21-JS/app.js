// console.log(sum);

// let sum = (a, b) => a + b;

// let result = sum(10, 20);

// console.log(result);

// console.log(sum(10, 20));
// console.log(sum(10, 20));
// console.log(sum(10, 60));

// let btn = document.querySelector("button");

// function doSomethingOnClick() {
//     console.log("Click!!!!!!!");
// }

// btn.addEventListener("click", doSomethingOnClick);

// Closures
// ===========================================

// Замыкание это механизм у функций получать доступ к внешним переменным

// let a = 1;
// function doSomething() {
//     let a = 10;
//     // console.log(a);

//     function fn() {
//         // let a = 100;
//         console.log(a);
//     }

//     fn();
// }
// doSomething();
// fn();

// function addOne(num) {
//     return function () {
//         console.log(1 + num);
//     };
// }

// let result = addOne(10);

// console.log(result());

// function incrementor(n) {
//     return function (num) {
//         return n + num;
//     };
// }

// let result = incrementor(10);
// console.log(result(500));

// let a = 1;
// let user = "Vova";

// function say() {
//     let b = 100;

//     console.log("Hello");

//     {
//         record: b;
//     }
// }

// {
//     record: say;
//     record: a;
//     recotd: user;
// }

// {
//     {
//         {
//             {

//             }
//         }
//     }
// }

// let userName = {
//     info: {
//         data: {
//             age: 25,
//         },
//     },
// };

// console.log(userName.info.data.age);

// window.say();
// say();

// function domainGenerator(domain) {
//     return function (url) {
//         return `https://${url}.${domain}`;
//     };
// }

// Carring // Карирование
// let netUtl = domainGenerator("net")("unian");
// let comUrl = domainGenerator("com")("facebook");
// console.log(netUtl);
// console.log(comUrl);

// domainGenerator()();

// let arr = [1, 2, 3, 4, 5];

// arr.push(10, 20, 30, 40);

// console.log(arr);

// function myPush() {}

// IIEF
// function sum(a, b) {
//     console.log(a + b);
// }
// sum(10, 65);

(function (a, b) {
    console.log(a + b);
})(10, 65);

(function (a, b) {
    console.log(a + b);
})(10, 65);

((name) => {
    console.log(name);
})("Vova");

((name) => {
    console.log(name);
})("Vova");
