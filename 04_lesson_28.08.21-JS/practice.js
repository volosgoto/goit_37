// const salary = [10000, 11500, 15000, 40000]; // 220 //
// let bonusSalary = salary.map(singleSalary => {
//   return singleSalary * 0.1 + singleSalary;
// });
// console.log(bonusSalary);

// let totalSalary = 0;
// bonusSalary.forEach(singleSalary => {
//   totalSalary += singleSalary;
// });
// console.log(totalSalary);
// split().reverse().join();
// map().reduce();
// let totalSalary = bonusSalary.reduce((total, elem, index, arr) => {
//   return total + elem;
// }, 0);
// console.log(totalSalary);

// let fullSalary = salary
//   .map(singleSalary => {
//     return singleSalary * 0.1 + singleSalary;
//   })
//   .reduce((total, elem, index, arr) => {
//     return total + elem;
//   }, 0);
// console.log(fullSalary);
// console.log(Array.isArray(fullSalary));

// salary.forEach((elem) => {
//   console.log(elem);
// });
// [].forEach();
// [].map();
// [].filter();
// [].find();
// [].every();
// [].some();
// [].sort();

// [].reduce();

/// задача БОТ ///

function StringBuilder(baseValue) {
    this.value = baseValue;
}

StringBuilder.prototype.getValue = function () {
    return this.value;
};

StringBuilder.prototype.padEnd = function (str) {
    this.value += str;
};

StringBuilder.prototype.padStart = function (str) {
    this.value = str + this.value;
};

StringBuilder.prototype.padBoth = function (str) {
    this.padStart(str);
    this.padEnd(str);
};

// Пиши код выше этой строки
const builder = new StringBuilder(".");
console.log(builder.getValue()); // '.'
builder.padStart("^");
console.log(builder.getValue()); // '^.'
builder.padEnd("^");
console.log(builder.getValue()); // '^.^'
builder.padBoth("=");
console.log(builder.getValue()); // '=^.^='
