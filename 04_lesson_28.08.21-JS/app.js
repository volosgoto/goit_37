// Loops
// for
// while
// do...while

// A - начало цикла
// B - Boolean => true
// C - продолжай цикл если Boolean === true

// for (A;B;C) {

// }

// let arr = [1, 2, 3, 4, 5];
// // console.log(arr[2]);

// let len = arr.length;
// let total = 0;

// for (let i = 0; i < len; i += 1) {
//     // console.log(arr[i] * 10);
//     total += arr[i];
// }
// console.log(total);

// =================================================
// [].ALLMETODS()
// [].method(function(){})
// [].method((elem, index, arr)=>{})

let tools = ["hammer", "saw", "screwdriwer", "axe"];
// let toolsToUpper = [];
// tools.forEach((elem, index, arr) => {
//     // console.log(elem);
//     // console.log(index);
//     // console.log(arr);
//     // console.log(elem.toUpperCase());
//     // toolsToUpper.push(elem.toUpperCase());
//
// });

// console.log(toolsToUpper);

// let toolsToUpper = tools.map((elem, index, arr) => {
//     return elem.toUpperCase();
// });

// let toolsToUpper = tools.map((elem) => elem.toUpperCase());
// console.log(toolsToUpper);

// Sum map
// let totalSalary = [11500, 25000, 40000, 45000];
// //

// let total = 0;
// let sumSalary = totalSalary.map((salary) => {
//     // return (total += salary);
//     total += salary;
//     return total;
// });

// // console.log(sumSalary[sumSalary.length - 1]);
// console.log(total);

// ===============================
// reduce
// [].reduce(callback, accumulator)
// [].reduce((accumulator, elem, index, arr) => {
//     // ...code
//     return accumulator;
// }, accumulator);

// accumulator - 100
// accumulator - 0
// accumulator - ''
// accumulator - []
// accumulator - {}

// let nums = [10, 20, 30, 40, 50];
// let result = nums.reduce((acc, elem, index, arr) => {
//     // console.log(acc);
//     // console.log(nums[nums.length - 1]);

//     // console.log(nums[index]);
//     if (index === nums.length - 1) {
//         return acc;
//     }
//     return acc + nums[index];
// }, 1000);

// console.log(result);

// Chaining
// split().reverse().join()
