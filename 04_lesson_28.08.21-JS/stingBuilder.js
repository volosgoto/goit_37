function StringBuilder(baseValue) {
    this.value = baseValue;
}

StringBuilder.prototype.getValue = function () {
    return this.value;
};

StringBuilder.prototype.padEnd = function (str) {
    this.value += str;
};

StringBuilder.prototype.padStart = function (str) {
    this.value = str + this.value;
};

StringBuilder.prototype.padBoth = function (str) {
    this.padStart(str);
    this.padEnd(str);
};

// Пиши код выше этой строки
const builder = new StringBuilder(".");
console.log(builder.getValue()); // '.'
builder.padStart("^");
console.log(builder.getValue()); // '^.'
builder.padEnd("^");
console.log(builder.getValue()); // '^.^'
builder.padBoth("=");
console.log(builder.getValue()); // '=^.^='

// =============================
// function User(name, age) {
//     // 1. {}
//     // 2. this
//     // 3. return {}
//     // console.log(name);
//     // console.log(age);
//     this.name = name;
//     this.age = age;

//     this.getInfo = function () {
//         console.log(`${this.name}, ${this.age}`);
//     };
// }

// // Instance
// let userVova = new User("Vova", 34);
// let userSara = new User("Sara", 25);

// // console.log(User);
// console.log(userVova);
// console.log(userSara);

// ES-6
class User {
    // 1. Вызывает Контсруктор
    // 2. Создает {}
    // 3. this
    // 4. return {}

    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    // getInfo() {
    //     console.log(`${this.name}, ${this.age}`);
    // getInfo.bind(this)
    // }

    // Public calss field
    pizza = "qwe";

    getInfo = () => {
        console.log(`${this.name}, ${this.age}`);
    };
}

let vova = new User("Vova", 34);
let sara = new User("Sara", 17);
console.log(vova.pizza);
console.log(vova.getInfo());
console.log(sara.pizza);
console.log(sara.getInfo());
