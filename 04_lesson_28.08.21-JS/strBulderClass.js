class StringBuilder {
    constructor(baseValue) {
        this.value = baseValue;
    }

    getValue() {
        return this.value;
    }

    padEnd(str) {
        this.value += str;
        return this;
    }

    padStart(str) {
        this.value = str + this.value;
        return this;
    }

    padBoth(str) {
        this.padStart(str);
        this.padEnd(str);
        return this;
    }

    // getThis() {
    //     console.log(this);
    // }
}

// Пиши код выше этой строки
const builder = new StringBuilder(".");
// const qwe = new StringBuilder("qwe");

// builder.getThis();
// qwe.getThis();
// console.log(builder.getValue()); // '.'
// builder.padStart("^");
// console.log(builder.getValue()); // '^.'
// builder.padEnd("^");
// console.log(builder.getValue()); // '^.^'
// builder.padBoth("=");
// console.log(builder.getValue()); // '=^.^='

// split().reverse().join();

builder.padStart("^").padEnd("^").padBoth("="); // =^.^=
console.log(builder.getValue());
