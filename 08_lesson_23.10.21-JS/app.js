class News {
    constructor(url, API_KEY) {
        this.API_KEY = API_KEY;
        this.url = url + this.API_KEY;
    }

    fetchNews = () => {
        fetch(this.url);
    };

    fetchNewsByAuthor = (authorName) => {
        let byAuthorUrl = this.url + "author" + authorName;
        fetch(byAuthorUrl);
    };

    fetchNewsByPopular = (newsName) => {
        fetch(this.url + "popular" + newsName);
    };

    init = () => {
        this.fetchNewsByAuthor("vova");
    };
}

let url =
    "https://content.guardianapis.com/search?page=2&q=debate&api-key=test&popular=coronavirus";
const API_KEY = "1234";

new News(url, API_KEY).init();
